#!/usr/bin/env sh
set -x
cd "$(dirname "$0")"
export env=${1:-dev}
export tag=${2:-$(git rev-parse --short HEAD)}

#Enable only for the first time to configure ingress controller
#helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
#helm upgrade --install ingress-$env --namespace lms-$env ingress-nginx/ingress-nginx
#kubectl patch service ingress-stage-ingress-nginx-controller -p '{"spec": {"type": "LoadBalancer", "externalIPs":["63.250.53.90"]}}'
#kubectl patch service -n istio-system istio-ingressgateway -p '{"spec": {"type": "LoadBalancer", "externalIPs":["63.250.53.90"]}}'
#To enable rewrite urls
#kubectl get cm istio-sidecar-injector -n istio-system -o yaml | sed -e 's/"rewriteAppHTTPProbe": false/"rewriteAppHTTPProbe": true/' | kubectl apply -f -

#kubectl patch service kiali -p '{"spec": {"type": "ClusterIP", "externalIPs":["63.250.53.90"]}}' -n istio-system
#kubectl patch service istio-ingressgateway -p '{"spec": {"type": "LoadBalancer", "externalIPs":["63.250.53.90"]}}' -n istio-system

helm install $env kibrit-ci-cd/ --namespace kibrit-ci-cd-$env  -f kibrit-ci-cd/$env.yaml --dry-run --debug

helm upgrade --install $env kibrit-ci-cd/ --namespace kibrit-ci-cd-$env -f kibrit-ci-cd/$env.yaml
