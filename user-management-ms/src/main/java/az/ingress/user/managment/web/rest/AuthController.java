package az.ingress.user.managment.web.rest;

import az.ingress.user.managment.dto.OtpRequestDto;
import az.ingress.user.managment.dto.OtpResponseDto;
import az.ingress.user.managment.service.OtpService;
import az.ingress.user.managment.service.SignUpService;
import az.ingress.user.managment.dto.SignUpRequestDto;
import az.ingress.user.managment.dto.SignUpResponseDto;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthController {

    private final SignUpService signUpService;
    private final OtpService otpService;

    @PostMapping("/sign-up")
    @ApiOperation(value = "Sign new mobile device", notes = "Creates and sends an otp code to the user's mobile phone")
    public SignUpResponseDto signUp(@RequestBody @Valid SignUpRequestDto dto) {
        log.trace("Sign up request with mobile number {}", dto.getMobileNumber());
        return signUpService.signUp(dto);
    }

    @PutMapping("/sca/{otpId}")
    @ApiOperation(value = "Verify otp", notes = "Verify the otp code sent to the user's mobile phone")
    public OtpResponseDto verifyOtp(@PathVariable Long otpId,
                                    @Valid @RequestBody OtpRequestDto otpRequestDto) {
        return otpService.verifyOtp(otpId, otpRequestDto);
    }

}
