package az.ingress.user.managment.service.impl;

import az.ingress.common.security.auth.services.JwtService;
import az.ingress.user.managment.dto.SignInDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JwtService jwtAccessTokenService;

    public String authenticate(SignInDto signInDto) {
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(signInDto.getPin(),
                signInDto.getPass());
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return jwtAccessTokenService.issueToken(authentication, Duration.ofDays(1));
    }
}
