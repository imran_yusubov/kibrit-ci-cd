package az.ingress.user.managment.dto;

import lombok.Data;

@Data
public class TokenRefreshResponseDto {

    private JwtAccessTokenDto accessToken;
    private RefreshTokenDto refreshToken;
}
