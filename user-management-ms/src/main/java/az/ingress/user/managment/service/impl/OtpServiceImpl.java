package az.ingress.user.managment.service.impl;

import az.ingress.common.exception.ApplicationException;
import az.ingress.user.managment.repository.OtpRepository;
import az.ingress.user.managment.service.OtpService;
import az.ingress.user.managment.config.OtpProperties;
import az.ingress.user.managment.domain.Otp;
import az.ingress.user.managment.dto.OtpRequestDto;
import az.ingress.user.managment.dto.OtpResponseDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Random;
import java.util.UUID;

import static az.ingress.user.managment.domain.enums.MethodType.SIGN_UP;
import static az.ingress.user.managment.domain.enums.OtpStatus.NEW;
import static az.ingress.user.managment.domain.enums.OtpStatus.VERIFIED;
import static az.ingress.user.managment.errors.Errors.OTP_EXPIRED;
import static az.ingress.user.managment.errors.Errors.OTP_IS_ALREADY_USED;
import static az.ingress.user.managment.errors.Errors.OTP_NOT_FOUND;
import static az.ingress.user.managment.errors.Errors.WRONG_OTP;

@Service
@RequiredArgsConstructor
public class OtpServiceImpl implements OtpService {

    private final OtpProperties otpProperties;
    private final OtpRepository otpRepository;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;

    @Override
    public OtpResponseDto verifyOtp(Long otpId, OtpRequestDto dto) {
        Otp otp = getOtp(otpId);
        validateOtp(otp, dto);
        checkOtpExpiration(otp);
        String tempPassword = generateRandomUserPassword();
        setTempPassword(otp, tempPassword);
        checkOtpStatus(otp);
        OtpResponseDto responseDto = modelMapper.map(otp, OtpResponseDto.class);
        responseDto.setOtpId(otp.getId());
        responseDto.setTemporaryUserPassword(tempPassword);
        return responseDto;
    }

    @Override
    public Otp generateOtp() {
        Otp otp = new Otp();
        String otpCode = String.format("%06d", generateOtpCode());
        otp.setOtp(otpCode);
        otp.setMethodType(SIGN_UP);
        otp.setOtpStatus(NEW);
        otp.setCreationTime(new Timestamp(System.currentTimeMillis()));
        otp.setOtpLifetime(otpProperties.getOtpLifeTime());
        return otpRepository.save(otp);
    }

    @Override
    public Otp getOtp(Long otpId) {
        return otpRepository.findById(otpId)
                .orElseThrow(() -> new ApplicationException(OTP_NOT_FOUND));
    }

    private int generateOtpCode() {
        Random random = new Random();
        return random.nextInt(999999);
    }

    private void validateOtp(Otp otp, OtpRequestDto dto) {
        if (!dto.getOtpCode().equals(otp.getOtp())) {
            throw new ApplicationException(WRONG_OTP);
        }
    }

    private void checkOtpExpiration(Otp otp) {
        int otpLifetime = otp.getOtpLifetime();
        long currentTimeInMillis = System.currentTimeMillis();
        long otpStartTimeMillis = otp.getCreationTime().getTime();
        if (otpStartTimeMillis + otpLifetime < currentTimeInMillis) {
            throw new ApplicationException(OTP_EXPIRED);
        }
    }

    private void checkOtpStatus(Otp otp) {
        if (otp.getOtpStatus() != NEW) {
            throw new ApplicationException(OTP_IS_ALREADY_USED);
        }
        otp.setOtpStatus(VERIFIED);
        otpRepository.save(otp);
    }

    private void setTempPassword(Otp otp, String tempPassword) {
        otp.setTemporaryUserPassword(passwordEncoder.encode(tempPassword));
        otpRepository.save(otp);
    }

    private String generateRandomUserPassword() {
        return UUID.randomUUID().toString();
    }

}
