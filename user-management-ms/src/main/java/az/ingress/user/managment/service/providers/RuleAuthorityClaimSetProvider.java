package az.ingress.user.managment.service.providers;

import az.ingress.common.security.auth.services.ClaimSet;
import az.ingress.common.security.auth.services.ClaimSetProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class RuleAuthorityClaimSetProvider implements ClaimSetProvider {

    private static final String RULE = "authority";

    @Override
    public ClaimSet provide(Authentication authentication) {
        log.trace("Providing claims");
        Set<String> authorities = authentication.getAuthorities().stream()
                .map(a -> a.getAuthority())
                .collect(Collectors.toSet());
        return new ClaimSet(RULE, authorities);
    }
}
