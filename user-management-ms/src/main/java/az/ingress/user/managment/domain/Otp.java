package az.ingress.user.managment.domain;

import az.ingress.user.managment.domain.enums.MethodType;
import az.ingress.user.managment.domain.enums.OtpStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

import static javax.persistence.EnumType.STRING;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Otp.TABLE_NAME)
public class Otp {
    public static final String TABLE_NAME = "otp";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String otp;

    @NotNull
    private Timestamp creationTime;

    @NotNull
    private int otpLifetime;

    @NotNull
    @Enumerated(STRING)
    private MethodType methodType;

    @NotNull
    @Enumerated(STRING)
    private OtpStatus otpStatus;

    private String temporaryUserPassword;

}
