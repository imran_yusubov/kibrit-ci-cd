package az.ingress.user.managment.errors;

import az.ingress.common.exception.ErrorResponse;
import org.springframework.http.HttpStatus;

public enum Errors implements ErrorResponse {

    AUTHENTICATION_FAILED("AUTHENTICATION_FAILED", HttpStatus.UNAUTHORIZED, "Authentication failed"),
    MOBILE_NUMBER_ALREADY_REGISTERED("MOBILE_NUMBER_ALREADY_REGISTERED", HttpStatus.BAD_REQUEST,
            "Given mobile number {number} is already registered"),
    USER_NOT_FOUND("USER_NOT_FOUND", HttpStatus.NOT_FOUND, "User not found with given id"),
    SIGN_UP_REQUEST_NOT_FOUND("SIGN_UP_NOT_FOUND", HttpStatus.NOT_FOUND, "Sign up request not found with given id"),
    WRONG_DEVICE_ID("WRONG_DEVICE_ID", HttpStatus.BAD_REQUEST, "Device id is wrong"),
    OTP_NOT_FOUND("OTP_NOT_FOUND", HttpStatus.NOT_FOUND, "Otp not found with given id"),
    OTP_EXPIRED("OTP_EXPIRED",HttpStatus.BAD_REQUEST,"Otp has already expired"),
    WRONG_OTP("WRONG_OTP", HttpStatus.BAD_REQUEST, "Otp is wrong"),
    OTP_IS_ALREADY_USED("OTP_IS_ALREADY_USED", HttpStatus.BAD_REQUEST, "Otp is already used");

    String key;
    HttpStatus httpStatus;
    String message;

    Errors(String key, HttpStatus httpStatus, String message) {
        this.message = message;
        this.key = key;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
