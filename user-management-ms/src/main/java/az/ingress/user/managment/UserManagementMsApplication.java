package az.ingress.user.managment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class UserManagementMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserManagementMsApplication.class, args);
    }

}
