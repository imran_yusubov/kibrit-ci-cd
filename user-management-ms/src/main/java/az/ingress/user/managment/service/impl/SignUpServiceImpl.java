package az.ingress.user.managment.service.impl;

import az.ingress.common.exception.ApplicationException;
import az.ingress.user.managment.domain.Otp;
import az.ingress.user.managment.service.OtpService;
import az.ingress.user.managment.domain.SignUp;
import az.ingress.user.managment.dto.SignUpRequestDto;
import az.ingress.user.managment.dto.SignUpResponseDto;
import az.ingress.user.managment.repository.SignUpRepository;
import az.ingress.user.managment.service.SignUpService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import static az.ingress.user.managment.errors.Errors.SIGN_UP_REQUEST_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final SignUpRepository signUpRepository;
    private final OtpService otpService;
    private final ModelMapper mapper;

    @Override
    public SignUpResponseDto signUp(SignUpRequestDto signUpDto) {
        SignUp signUp = mapper.map(signUpDto, SignUp.class);
        Otp otp = otpService.generateOtp();
        signUp.setOtp(otp);
        SignUp savedSignUp = signUpRepository.save(signUp);
        return fillSignUpResponseDto(savedSignUp);
    }

    @Override
    public SignUp getSignUp(Long signUpId) {
        return signUpRepository.findById(signUpId)
                .orElseThrow(() -> new ApplicationException(SIGN_UP_REQUEST_NOT_FOUND));
    }

    private SignUpResponseDto fillSignUpResponseDto(SignUp savedSignUp) {
        SignUpResponseDto responseDto = new SignUpResponseDto();
        responseDto.setMobileNumber(savedSignUp.getMobileNumber());
        responseDto.setOtpId(savedSignUp.getOtp().getId());
        responseDto.setOtpLifetime(savedSignUp.getOtp().getOtpLifetime());
        return responseDto;
    }

}
