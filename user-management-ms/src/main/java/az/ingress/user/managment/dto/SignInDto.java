package az.ingress.user.managment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignInDto {

    @NotNull
    @Size(min = 7, max = 7)
    private String pin;

    @NotNull
    private String pass;

    private String deviceToken;

}
