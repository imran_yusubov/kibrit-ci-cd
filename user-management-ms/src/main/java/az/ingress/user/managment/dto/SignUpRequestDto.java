package az.ingress.user.managment.dto;

import az.ingress.user.managment.validation.ValidPhone;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignUpRequestDto {

    @NotBlank
    @ValidPhone
    @ApiModelProperty(example = "994556420943")
    private String mobileNumber;

    @NotBlank
    @ApiModelProperty(example = "89ABCDEF-01234567-89ABCDEF")
    private String deviceId;
}
