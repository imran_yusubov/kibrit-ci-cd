package az.ingress.user.managment.config;

import az.ingress.common.security.config.ApplicationSecurityConfigurer;
import lombok.SneakyThrows;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
@ComponentScan("az.ingress.common.security")
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfigurer implements ApplicationSecurityConfigurer {

    @Override
    @SneakyThrows
    public void configure(HttpSecurity http) {
        http.authorizeRequests()
                .antMatchers(
                        "/auth/sign-up",
                        "/auth/sca/**")
                .permitAll()
                .antMatchers("/v1/vpl/transfer").hasAnyAuthority("VISA");
    }
}
