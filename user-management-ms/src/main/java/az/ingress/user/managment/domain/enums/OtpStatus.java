package az.ingress.user.managment.domain.enums;

public enum OtpStatus {
    NEW,
    VERIFIED
}
