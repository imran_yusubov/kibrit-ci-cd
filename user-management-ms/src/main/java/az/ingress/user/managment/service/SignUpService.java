package az.ingress.user.managment.service;

import az.ingress.user.managment.domain.SignUp;
import az.ingress.user.managment.dto.SignUpRequestDto;
import az.ingress.user.managment.dto.SignUpResponseDto;

public interface SignUpService {

    SignUpResponseDto signUp(SignUpRequestDto signUpDto);

    SignUp getSignUp(Long signUpId);

}
