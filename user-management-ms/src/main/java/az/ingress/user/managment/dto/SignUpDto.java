package az.ingress.user.managment.dto;

import az.ingress.user.managment.validation.ValidPhone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignUpDto {

    @NotBlank
    @Size(min = 7, max = 7)
    private String pin;

    @NotBlank
    @Size(min = 7, max = 11)
    private String idCardCode;

    @NotBlank
    @ValidPhone
    private String phone;

}
