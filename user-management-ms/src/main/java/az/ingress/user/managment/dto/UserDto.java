package az.ingress.user.managment.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private Long id;

    @NotNull
    private String about;
    private String phone;

    @Email
    @NotNull
    private String email;
    private Long organisationId;
    private String settings;
    private String photo;
    private String displayName;
    private VerificationTokenDto verificationTokenDto;

}
