package az.ingress.user.managment.domain;

import az.ingress.user.managment.validation.ValidPhone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = SignUp.TABLE_NAME)
public class SignUp {

    public static final String TABLE_NAME = "user_sign_up_requests";
    private static final long serialVersionUID = 2871215574426241619L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ValidPhone
    @Column(unique = true, nullable = false)
    private String mobileNumber;

    @Column(nullable = false)
    private String deviceId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "otp_id", referencedColumnName = "id")
    private Otp otp;

}
