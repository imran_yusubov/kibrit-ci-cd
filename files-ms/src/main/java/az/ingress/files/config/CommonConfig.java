package az.ingress.files.config;

import az.ingress.common.config.ModelMapperConfig;
import az.ingress.common.config.SpringFoxConfig;
import az.ingress.common.exception.GlobalExceptionHandler;
import az.ingress.common.logging.LoggingTcpConfiguration;
import az.ingress.common.security.config.SwaggerConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Import({ModelMapperConfig.class, GlobalExceptionHandler.class,
        LoggingTcpConfiguration.class, SpringFoxConfig.class, SwaggerConfig.class})
@Configuration
public class CommonConfig {

}
